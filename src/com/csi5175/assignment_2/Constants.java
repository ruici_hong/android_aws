package com.csi5175.assignment_2;

public class Constants {

	public static final String AWS_ACCOUNT_ID = "594940692705";
	public static final String COGNITO_POOL_ID = "us-east-1:be16e591-38a2-416c-be88-270f4e1f8615";
	public static final String COGNITO_ROLE_UNAUTH = "arn:aws:iam::594940692705:role/Cognito_assignment2Unauth_Role";
	// Note, the bucket will be created in all lower case letters
	// If you don't enter an all lower case title, any references you add
	// will need to be sanitized
	public static final String BUCKET_NAME = "mobileassignment2";
}
