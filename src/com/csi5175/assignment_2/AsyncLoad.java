package com.csi5175.assignment_2;

import java.util.ArrayList;
import java.util.Arrays;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class AsyncLoad{	

	public static int defaultLoadStyle = -1;
	public static int defaultLoadResource= -1;	
	private AsyncLoadListener listener;	
	private static AsyncLoad load;
	
	private AsyncLoad(){
	}
	
	@Deprecated
	protected static synchronized AsyncLoad getInstance(AsyncLoadListener listener){
		if(load==null){
			load = new AsyncLoad();
		}
		load.listener = listener;
		return load;
	}
	

	public static AsyncLoad getAloneInstance(AsyncLoadListener listener){
		AsyncLoad load = new AsyncLoad();
		load.listener = listener;
		return load;
	}
	

	public void load(Context context,final boolean hasLoding,Integer... loadCodes) {
		final ProgressDialog dialog = new ProgressDialog(context);
		if(hasLoding){
			dialog.setMessage("data is loading,please wait...");
			
			if(defaultLoadResource != -1){
				dialog.setContentView(defaultLoadResource);
			}			
			if(defaultLoadStyle !=-1){
				dialog.setIndeterminateDrawable(context.getResources().getDrawable(defaultLoadStyle));
			}	
			dialog.setCancelable(false);
			dialog.show();
		}
		final ArrayList<Integer> list =  new ArrayList<Integer>(Arrays.asList(loadCodes));
		int j = loadCodes.length;
		for(int i=0;i<j;i++){
	    	final int loadCode = loadCodes[i];
			new AsyncTask<String, Integer, Object>() {	
				protected Object doInBackground(String... params) {
					if(listener != null){
					  return listener.doInBackground(loadCode);
					}else{
						return null;
					}
					
				}
				@Override
				protected void onPostExecute(Object result) {
					if(hasLoding){
						if(dialog.isShowing()){
							list.remove(0);
							if(list.size()==0){
								dialog.dismiss();
							}
						}else{
							return;
						}
					}
					if(listener != null){
						listener.doInUI(result,loadCode);
					}
				}
			}.execute("");
	    }
		
	}
	

	public void loadunCancel(Context context,final boolean hasLoding,Integer... loadCodes) {
		final ProgressDialog dialog = new ProgressDialog(context);
		if(hasLoding){
			dialog.setMessage("data is loading,please wait...");
			
			if(defaultLoadResource != -1){
				dialog.setContentView(defaultLoadResource);
			}			
			if(defaultLoadStyle !=-1){
				dialog.setIndeterminateDrawable(context.getResources().getDrawable(defaultLoadStyle));
			}	
			dialog.setCancelable(false);
			dialog.show();
		}
		final ArrayList<Integer> list =  new ArrayList<Integer>(Arrays.asList(loadCodes));
		int j = loadCodes.length;
		for(int i=0;i<j;i++){
	    	final int loadCode = loadCodes[i];
			new AsyncTask<String, Integer, Object>() {	
				protected Object doInBackground(String... params) {
					if(listener != null){
					  return listener.doInBackground(loadCode);
					}else{
						return null;
					}
					
				}
				@Override
				protected void onPostExecute(Object result) {
					if(hasLoding){
						if(dialog.isShowing()){
							list.remove(0);
							if(list.size()==0){
								dialog.dismiss();
							}
						}else{
							return;
						}
					}
					if(listener != null){
						listener.doInUI(result,loadCode);
					}
				}
			}.execute("");
	    }
		
	}
	

	public void load(Context context,Integer...loadCodes) {
	    load(context, true, loadCodes);
	}
	

	public static void setDefaultLoadStyle(int defaultLoadStyle) {
		AsyncLoad.defaultLoadStyle = defaultLoadStyle;
	}
     

	public static void setDefaultLoadResource(int defaultLoadResource) {
		AsyncLoad.defaultLoadResource = defaultLoadResource;
	}
	

	public void setListener(AsyncLoadListener listener) {
		this.listener = listener;
	}
	
	
	public interface AsyncLoadListener {
	
		Object doInBackground(int loadCode);	
		
		
		void doInUI(Object result,int loadCode);
		
	}

}
