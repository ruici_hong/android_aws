package com.csi5175.assignment_2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.com.google.gson.Gson;
import com.amazonaws.com.google.gson.JsonNull;
import com.amazonaws.mobileconnectors.s3.transfermanager.*;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.csi5175.assignment_2.AsyncLoad.AsyncLoadListener;

public class EditNote extends Activity implements  AsyncLoadListener, OnClickListener {

	private EditText et_title;
	private EditText et_body;
	private EditText et_label;
	private EditText et_latitude;
	private EditText et_longitude;
	private Button btn_confirm;
	private static final int INSERT_ID = Menu.FIRST;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_edit_note);
		init();
	}
	

    
	public void init(){
		et_title = (EditText)findViewById(R.id.et_title);
		et_body = (EditText)findViewById(R.id.et_body);
		et_label = (EditText)findViewById(R.id.et_label);
		et_latitude = (EditText)findViewById(R.id.et_latitude);
		et_longitude = (EditText)findViewById(R.id.et_longitude);
		btn_confirm = (Button)findViewById(R.id.btn_confirm);
		btn_confirm.setOnClickListener(this);
		Item item = (Item) getIntent().getSerializableExtra("ITEM");
		if(item!=null){
			et_title.setText(item.getTitle());
			et_body.setText(item.getBody());
			et_label.setText(item.getLabel());
			et_latitude.setText(item.getLat()+"");
			et_longitude.setText(item.getLon()+"");
		}
		
	}

	@Override
	public void onClick(View v) {
		
		switch(v.getId()){
		case R.id.btn_confirm:		
			if(TextUtils.isEmpty(et_title.getText())){
				Toast.makeText(EditNote.this, "title can not null", Toast.LENGTH_SHORT).show();
				return ;
			}
			if(TextUtils.isEmpty(et_body.getText())){
				Toast.makeText(EditNote.this, "body can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_label.getText())){
				Toast.makeText(EditNote.this, "label can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_latitude.getText())){
				Toast.makeText(EditNote.this, "latitude can not null", Toast.LENGTH_SHORT).show();
				return ;
			}if(TextUtils.isEmpty(et_longitude.getText())){
				Toast.makeText(EditNote.this, "longitude can not null", Toast.LENGTH_SHORT).show();
				return ;
			}
			AsyncLoad.getAloneInstance(EditNote.this).load(EditNote.this,1);	
		break;
		
		default: 
		break;
		}
		
	}
	
	
//	private class CreateBucket extends AsyncTask<Object, Void, Boolean> {
//
//        @Override
//        protected Boolean doInBackground(Object... params) {
//        	
////            File file = new File(
////                    Environment.getExternalStoragePublicDirectory(
////                            Environment.DIRECTORY_PICTURES),
////                    "first.");
////        	TransferManager transferManager = new TransferManager(Util.getCredProvider(getApplicationContext()));
//            AmazonS3Client sS3Client = Util.getS3Client(getApplicationContext());
//           List<Bucket> list =  sS3Client.listBuckets();
//           String name = list.get(0).getName();
//          System.out.println("name>>>>>>>>"+name);
//          /** Getting Cache Directory */
//          File cDir = getBaseContext().getCacheDir();
//          /** Getting a reference to temporary file, if created earlier */
//         File tempFile = new File(cDir.getPath() + "/" + "reterter") ;
//   
//          FileWriter writer=null;
//          try {
//        	  
//        	  
//        	  
//        	  
//        	  
//              writer = new FileWriter(tempFile);
//
//              /** Saving the contents to the file*/
//              writer.write("lat 1233434"
//              		+ "lon 43242352.00"
//            		  +"body rtrtwrw"+"title reterter"
//              		);
//
//              /** Closing the writer object */
//              writer.close();
//
//           
//
//          } catch (IOException e) {
//              e.printStackTrace();
//          }
////          
//          sS3Client.putObject("mobileassignment2", "upload", tempFile);
//            
//          
//          
//          
//          
//          
//          
//          
//          
//           S3ObjectInputStream s = sS3Client.getObject("mobileassignment2", "da.txt").getObjectContent();
//        
//           try {
//			System.out.println("name>>>>>>>>"+getContent(s));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//            if (!Util.doesBucketExist()) {
//                Util.createBucket();
//                return true;
//            }
//            return false;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean result) {
//            if (!result) {
//                Toast.makeText(getApplicationContext(), "Bucket already exists", Toast.LENGTH_SHORT)
//                        .show();
//            }
//            else {
//                Toast.makeText(getApplicationContext(), "Bucket successfully created!",
//                        Toast.LENGTH_SHORT).show();
//            }
////            exists = true;
//        }
//    }
	
	private static String getContent(S3ObjectInputStream fin) throws IOException {
	    int ch;
	    StringBuilder builder = new StringBuilder();
	    while ((ch = fin.read()) != -1) {
	        builder.append((char) ch);
	    }
	    return builder.toString();
	}

	@Override
	public Object doInBackground(int loadCode) {
		int respCode = 0;
		String title = et_title.getText().toString();
		String body = et_body.getText().toString();
		String label = et_label.getText().toString();
		String lat = et_latitude.getText().toString();
		String lon = et_longitude.getText().toString();
		JSONObject obj = new JSONObject();
		try {
			obj.put("title", title);
			obj.put("body", body);
			obj.put("label", label);
			obj.put("lat", lat);
			obj.put("lon", lon);
			
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
  	
	    AmazonS3Client sS3Client = Util.getS3Client(getApplicationContext());
	    /** Getting Cache Directory */
        File cDir = getBaseContext().getCacheDir();
 
        /** Getting a reference to temporary file, if created earlier */
       File tempFile = new File(cDir.getPath() + "/" + title) ;
 
        FileWriter writer=null;
        try {
            writer = new FileWriter(tempFile);
            /** Saving the contents to the file*/
            writer.write(obj.toString());
            /** Closing the writer object */
            writer.close();
            sS3Client.putObject("mobileassignment2", title+".txt", tempFile);
            respCode =1;
        } catch (Exception e) {
            e.printStackTrace();
            respCode =2;
        }  
    	return respCode;
	}

	@Override
	public void doInUI(Object result, int loadCode) {
	    int intResult = (Integer) result;
		System.out.println(intResult);
			switch (intResult) {
			case  0:
			
			break;
			case  1:
				Toast.makeText(EditNote.this, "The file upload successfully", Toast.LENGTH_SHORT).show();
				EditNote.this.setResult(RESULT_OK);
				EditNote.this.finish();
				break;
			case  2:
				Toast.makeText(EditNote.this, "The provided token has expired", Toast.LENGTH_SHORT).show();
				break;
			
			}
		
	}
}
