package com.csi5175.assignment_2;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.com.google.gson.Gson;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.csi5175.assignment_2.AsyncLoad.AsyncLoadListener;
import com.csi5175.assignment_2.RefreshableView.PullToRefreshListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements  AsyncLoadListener, OnItemClickListener{
	RefreshableView refreshableView;
	ListView listView;
	ArrayAdapter<String> adapter;
	AmazonS3Client sS3Client ;
	private TextView tv_empty;
	private static final int INSERT_ID = Menu.FIRST;
	private static final int VIEW_MAP = Menu.FIRST+1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		sS3Client = Util.getS3Client(getApplicationContext());
		refreshableView = (RefreshableView) findViewById(R.id.refreshable_view);
		tv_empty = (TextView)refreshableView.findViewById(R.id.tv_empty);
		listView = (ListView) findViewById(R.id.list_view);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		AsyncLoad.getAloneInstance(MainActivity.this).load(MainActivity.this,1);
		refreshableView.setOnRefreshListener(new PullToRefreshListener() {
			@Override
			public void onRefresh() {
				try {
					getItemsFromS3();
				} catch (Exception e) {
					e.printStackTrace();
				}
				refreshableView.finishRefreshing();
			}
		}, 0);
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, INSERT_ID, 0, R.string.menu_insert);
        menu.add(0, VIEW_MAP, 0, R.string.menu_view_map);
        return true;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch(item.getItemId()) {
            case INSERT_ID:
    			Intent mIntent = new Intent(MainActivity.this, EditNote.class);
				startActivityForResult(mIntent, 1);
                return true;
			case VIEW_MAP:
	
				// sS3Client.get
				//
		
				AsyncLoad.getAloneInstance(new AsyncLoadListener() {
	
					@Override
				public void doInUI(Object result, int loadCode) {
					if(result!=null){
						 Intent m1Intent = new Intent(MainActivity.this,MapsActivity.class);
						 m1Intent.putExtra("LOCATIONS", (Serializable)result);
						 startActivity(m1Intent);
					}
					else{
						Toast.makeText(MainActivity.this, "Loading Google Map Fail, please try again", Toast.LENGTH_SHORT).show();
					}
						

				}
	
					@Override
					public Object doInBackground(int loadCode) {
						List Itemlist = new ArrayList<Item>();
						try {
							ObjectListing ol = sS3Client.listObjects(Constants.BUCKET_NAME);
							List<S3ObjectSummary> list = ol.getObjectSummaries();
							System.out.println("list size =="+list.size());
						
							for (int i = 0; i < list.size(); i++) {
								S3ObjectInputStream s = sS3Client.getObject(Constants.BUCKET_NAME,list.get(i).getKey()).getObjectContent();
								Gson gson = new Gson();
								String val="";
								val = getContent(s);
								Item item = gson.fromJson(val, Item.class);
								Itemlist.add(item);
								
							}
						} catch (Exception e) {
							e.printStackTrace();
							return null;
						}
						return Itemlist;
					}
				}).load(MainActivity.this, 1);
	
				return true;
	        }

        return super.onMenuItemSelected(featureId, item);
    }

	@Override
	public Object doInBackground(int loadCode) {
		try{
		getItemsFromS3();	
		}
		catch(Exception e){
		e.printStackTrace();
		refleshHandler.sendEmptyMessage(1);	
		}
		return null;
	}

	@Override
	public void doInUI(Object result, int loadCode) {
		
	}


	public void getItemsFromS3(){
	  
	    ObjectListing ol = sS3Client.listObjects(Constants.BUCKET_NAME);
	    List<S3ObjectSummary> list = ol.getObjectSummaries();
	    String[] items = new String[list.size()];
	    for(int i=0;i<list.size();i++){
	    items[i] = list.get(i).getKey();
	    }		   
	   Message msg = new Message();
	   msg.what = 0;
	   Bundle data = new Bundle();
	   data.putStringArray("keys", items);
	   msg.setData(data);
	   refleshHandler.sendMessage(msg);	
	}
	
	private Handler refleshHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				try{
					Bundle data =	msg.getData();
					String[] items =  data.getStringArray("keys");
					adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, items);
					if(items!=null&&items.length==0){
						tv_empty.setText("NO Note Yet");
					}else{
						tv_empty.setText("");
					}
					listView.setAdapter(adapter);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				break;
			case 1:
				
				Toast.makeText(MainActivity.this, "Loading Item List Fail, please try again", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	@Override
	public void onItemClick(final AdapterView<?> parent, View view, final int position,
			long id) {
		AsyncLoad.getAloneInstance(new AsyncLoadListener(){

			@Override
			public Object doInBackground(int loadCode) {
				Item item  = null;
				String key = parent.getAdapter().getItem(position).toString();
				try {
					S3ObjectInputStream s = sS3Client.getObject(Constants.BUCKET_NAME,
							key).getObjectContent();
					Gson gson = new Gson();
					String val = getContent(s);
					 item = gson.fromJson(val, Item.class);
				} catch (Exception e) {
				
					e.printStackTrace();
				}	
	
				return item;
			}

			@Override
			public void doInUI(Object result, int loadCode) {
				if (result != null) {
					Item item = (Item) result;
					Intent mIntent = new Intent(MainActivity.this, EditNote.class);
					Bundle mBundle = new Bundle();
					mBundle.putSerializable("ITEM", item);
					mIntent.putExtras(mBundle);
					startActivityForResult(mIntent, 1);

				}
				else{
					Toast.makeText(MainActivity.this, "Loading Item fail, please try again", Toast.LENGTH_SHORT).show();
				}

			}
		}).load(MainActivity.this, 1);
	}
	
	private static String getContent(S3ObjectInputStream fin) throws IOException {
	    int ch;
	    StringBuilder builder = new StringBuilder();
	    while ((ch = fin.read()) != -1) {
	        builder.append((char) ch);
	    }
	    return builder.toString();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK){
		AsyncLoad.getAloneInstance(MainActivity.this).load(MainActivity.this,1);	
		}	
	}
}
