package com.csi5175.assignment_2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.view.Window;

public class MapsActivity extends Activity {
	private GoogleMap mMap;
	private Location location;
	private List<Item> list = new ArrayList<Item>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_map);
		mMap =((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.getUiSettings().setZoomControlsEnabled(true);   
System.out.println("fdsfdsf");
        String lat = "51.382948";
        String lng = "-2.3607617";
        list =  (List) this.getIntent().getSerializableExtra("LOCATIONS");
		if (list != null) {
//			Iterator iter = hashmap.entrySet().iterator();
//			while (iter.hasNext()) {
//				Map.Entry entry = (Map.Entry) iter.next();
//				Object key = entry.getKey();
//				Item val = (Item) entry.getValue();

//			}
			for(Item item:list){
				drawMarker(item.getLabel(),
				new LatLng(item.getLat(), item.getLon()));
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	private void drawMarker(String title,LatLng point) {
		// Creating an instance of MarkerOptions
		MarkerOptions markerOptions = new MarkerOptions();

		// Setting latitude and longitude for the marker
		markerOptions.position(point);
		markerOptions.title(title);
		// Adding marker on the Google Map
		mMap.addMarker(markerOptions);
	}
}