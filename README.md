# README #

This README would normally document whatever steps are necessary to get your application up and running.

* android:minSdkVersion=“17"

### Extend Notepad Tutorial to use AWS ###

* Instead of storing the notes in a SQLite database stored locally on the Android machine, this project store them in S3. Each note is its own text file in S3.

### Integrating Android application with Google Maps ###

* Modify the Notepad application above to store GPS locations instead of notes. The view for the “Edit Notes” activity now accept a label, a latitude value, and a longitude value, from the user, instead of simply a title and a body.
* A button (“View map”) from the main page of the application which opens a new view which displays the locations of all the labels in the list on Google maps.

![Screen Shot 2015-11-18 at 10.06.34 PM.png](https://bitbucket.org/repo/4qjA8d/images/3705058387-Screen%20Shot%202015-11-18%20at%2010.06.34%20PM.png)

![Screen Shot 2015-11-18 at 10.06.49 PM.png](https://bitbucket.org/repo/4qjA8d/images/2468072594-Screen%20Shot%202015-11-18%20at%2010.06.49%20PM.png)![Screen Shot 2015-11-18 at 10.07.10 PM.png](https://bitbucket.org/repo/4qjA8d/images/427822886-Screen%20Shot%202015-11-18%20at%2010.07.10%20PM.png)![Screen Shot 2015-11-18 at 10.07.30 PM.png](https://bitbucket.org/repo/4qjA8d/images/2618620047-Screen%20Shot%202015-11-18%20at%2010.07.30%20PM.png)![Screen Shot 2015-11-18 at 10.07.40 PM.png](https://bitbucket.org/repo/4qjA8d/images/747938308-Screen%20Shot%202015-11-18%20at%2010.07.40%20PM.png)